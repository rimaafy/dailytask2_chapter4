// imports
const express = require('express');
const app = express()
const PORT = 8000
const datas = require('./data');
// import data
const {data1, data2, data3, data4, data5} = require("./function");
// set view engine to ejs
app.set("view engine", "ejs")
// file static
app.use(express.static("public"))
// menampilkan home
app.get("/",(req,res)=>{
    res.render('home')
})
// menampilkan about
app.get("/about",(req,res)=>{
    res.render('about')
})
  // menampilkan semua data di table (ejs)
  app.get('/data', (req, res) => {
    res.render('table', {
      output: datas,
      pesan:'Menampilkan semua data'
    });
  });
  
  // menampilkan data filter di table.ejs
  app.get('/data1', (req, res) => {
    res.render('table', {
      output: data1(),
      pesan:'Age dibawah 30 tahun dan favorite fruit pisang'
    });
  });
  
  app.get('/data2', (req, res) => {
    res.render('table', {
      output: data2(),
      pesan:'Gender female atau company FSW4 dan age diatas 30 tahun'
    });
  });
  
  app.get('/data3', (req, res) => {
    res.render('table', {
      output: data3(),
      pesan:'Eye color biru dan age diantara 35 sampai dengan 40, dan favorite fruit apel'
    });
  });
  
  app.get('/data4', (req, res) => {
    res.render('table', {
      output: data4(),
      pesan:'Company Pelangi atau Intel, dan eye color hijau'
    });
  });
  
  app.get('/data5', (req, res) => {
    res.render('table', {
      output: data5(),
      pesan:'Registered di bawah tahun 2016 dan masih active(true)'
    });
  });
  
  // set tampilan default apa pun url-nya
  app.use("/", (req, res) => {
    res.render('404');
  });

app.listen(PORT, () =>{
console.log(`Server run on http://localhost:${PORT}`);
})
